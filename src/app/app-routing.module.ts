import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AgregarAgendaComponent } from './components/recetario/agregar-receta/agregar-receta.component';
import { RecetarioComponent } from './components/recetario/recetario.component';
import { VerAgendaComponent } from './components/recetario/ver-receta/ver-receta.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  // { path: 'datos', component: DatosComponent },
 
  {path:'agenda',component:RecetarioComponent},
  
  {path:'agregar-agenda/:id',component:AgregarAgendaComponent},
  {path:'ver-cita/:id',component:VerAgendaComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
